package fr.zaraakai;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public class Hat extends JavaPlugin implements Listener{
	@Override
	public void onEnable() {
		System.out.println("[HAT] ENABLE !");
		saveDefaultConfig();
	}
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(command.getName().equalsIgnoreCase("hat")){
			Player p = (Player)sender;
			if(p.hasPermission("hat.set")){
				p.getInventory().setHelmet(p.getItemInHand());
				p.sendMessage(getConfig().getString("succeshat").replace("&", "§").replace("%PLAYER%", p.getPlayer().getName()));
			}else{
				p.sendMessage(getConfig().getString("nopermsmessage").replace("&", "§").replace("%PLAYER%", p.getPlayer().getName()));
			}
		}
		return false;
	}

}
